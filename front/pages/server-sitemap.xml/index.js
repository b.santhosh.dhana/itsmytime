import { getServerSideSitemap } from "next-sitemap";
import axios, { localHost } from "../../axios.config";

export const getServerSideProps = async (ctx) => {
  const { data } = await axios.get("blog", { limit: 50 });
  const fields = data.map((item) => ({
    loc: `${localHost}/article/${item.slug}`,
    lastmod: new Date().toISOString(),
  }));
  return getServerSideSitemap(ctx, fields);
};

export default () => {};

import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.scss";
import Footer from "../components/Footer";
import NavBar from "../components/NavBar";
import { useEffect } from "react";

const hidePaths = ["article"];

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    import("bootstrap/dist/js/bootstrap");
  }, []);

  return (
    <div>
      <NavBar hidePaths={hidePaths} />
      <Component {...pageProps} />
      <Footer />
      <script
        type="text/javascript"
        src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-619bc7f5c16a8b6d"
      ></script>
    </div>
  );
}

export default MyApp;

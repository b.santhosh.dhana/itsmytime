import Head from "next/head";
import Hero from "../components/Hero";
import Featured from "../components/Featured";
import axios from "../axios.config";

export default function Home({ data }) {
  return (
    <>
      <Head>
        <title>zero read</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Hero />
      <Featured posts={data} />
    </>
  );
}

export async function getStaticProps(context) {
  let { data } = await axios.get("blog");
  return {
    props: { data },
  };
}

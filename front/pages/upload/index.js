import styles from "./styles.module.scss";
import axios from "../../axios.config";
import Image from "next/image";
import React, { useState, useCallback } from "react";

export default function upload() {
  const [mobileVersionImages, setMobileVersionImages] = useState([]);
  const [desktopVersionImages, setDesktopVersionImages] = useState("");
  const [fontStyle, setFontStyle] = useState("");
  const [title, setTitle] = useState("");
  const [markDown, setMarkDown] = useState("");
  const [secretKey, setSecretKey] = useState("");
  const [message, setMessage] = useState("");
  const [error, setError] = useState("");
  const [data, setData] = useState("");
  const [loading, setLoading] = useState(false);

  const handleMobileImages = (e) => {
    const newImages = e.target.files;
    if (newImages) {
      setMobileVersionImages([...mobileVersionImages, ...newImages]);
    }
  };

  const removeMobileVersionImages = useCallback(
    (index) => {
      mobileVersionImages.splice(index, 1);
      setMobileVersionImages([...mobileVersionImages]);
    },
    [mobileVersionImages]
  );

  const handleDesktopImages = (e) => {
    const newImages = e.target.files;
    if (newImages) {
      setDesktopVersionImages([...desktopVersionImages, ...newImages]);
    }
  };

  const removeDesktopVerionImages = useCallback(
    (index) => {
      desktopVersionImages.splice(index, 1);
      setDesktopVersionImages([...desktopVersionImages]);
    },
    [desktopVersionImages]
  );

  const handleSubmit = () => {
    setLoading(true);
    const formData = new FormData();
    formData.append("title", title);
    formData.append("fontStyle", fontStyle);
    formData.append("secretKey", secretKey);
    formData.append("markDown", markDown);
    for (let img of mobileVersionImages) {
      formData.append("mobileVersionImages", img);
    }
    for (let img of desktopVersionImages) {
      formData.append("desktopVersionImages", img);
    }
    let config = {
      method: "post",
      url: "blog/upload",
      data: formData,
      headers: { "Content-Type": "multipart/form-data" },
    };
    axios(config)
      .then((res) => {
        setLoading(false);
        setMessage("Succesfully uploaded");
        setData(res.data);
        setError("");
      })
      .catch((err) => {
        setError(err.response.data);
        setLoading(false);
        setMessage("");
      });
  };

  return (
    <div className="container">
      {error && (
        <div className="alert alert-danger my-3" role="alert">
          {error}
        </div>
      )}
      {message && (
        <div className="alert alert-success my-3" role="alert">
          {message}
        </div>
      )}

      <div className="form-group my-3">
        <label htmlFor="title">Title</label>
        <input
          id="title"
          type="text"
          className="form-control"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div className="form-group my-3">
        <label htmlFor="markup">MarDown</label>
        <textarea
          id="markup"
          type="text"
          className="form-control"
          rows="9"
          value={markDown}
          onChange={(e) => setMarkDown(e.target.value)}
        />
      </div>
      <div className="form-row">
        <div className="form-group col-md-6 my-3">
          <label htmlFor="snaps">Mobile Version Images</label>
          <input
            type="file"
            multiple
            id="snaps"
            onChange={handleMobileImages}
            className="form-control"
          />
        </div>
        <div className="form-group col-md-6 my-3">
          <label htmlFor="snapsD">Desktop Version Images</label>
          <input
            type="file"
            multiple
            id="snapsD"
            onChange={handleDesktopImages}
            className="form-control"
          />
        </div>
        <div className="form-group col-md-4 my-3">
          <label htmlFor="fontStyle">FontStyle</label>
          <select
            id="fontStyle"
            className="form-control"
            onChange={(e) => setFontStyle(e.target.value)}
          >
            <option defaultValue="Roboto" value="Roboto">
              Roboto
            </option>
            <option value="Pacifico">Pacifico</option>
            <option value="Permanent Marker">Permanent Marker</option>
            <option value="Supermercado One">Supermercado One</option>
          </select>
        </div>
        <div className="form-group col-md-4 my-3">
          <label htmlFor="title">Key</label>
          <input
            id="key"
            type="text"
            className="form-control"
            value={secretKey}
            onChange={(e) => setSecretKey(e.target.value)}
          />
        </div>
      </div>
      <button className="btn br-5 btn-primary" onClick={handleSubmit}>
        {loading ? (
          <>
            <div
              className="spinner-border align-middle text-light"
              role="status"
            ></div>
            <span className="align-middle px-4">Loading...</span>
          </>
        ) : (
          "Submit"
        )}
      </button>
      <SelectedImages
        {...{
          mobileVersionImages,
          desktopVersionImages,
          removeMobileVersionImages,
          removeDesktopVerionImages,
        }}
      />
      <div>{data && <pre>{JSON.stringify(data, null, 2)}</pre>}</div>
    </div>
  );
}

const SelectedImages = React.memo(
  ({
    mobileVersionImages,
    desktopVersionImages,
    removeMobileVersionImages,
    removeDesktopVerionImages,
  }) => {
    return (
      <>
        {mobileVersionImages.length > 0 && (
          <>
            <h3 className="text-secondary mt-5 ">Mobile Version Photos</h3>
            <div className={styles.imageWrapper}>
              {mobileVersionImages.map((img, index) => {
                return (
                  <div
                    className="position-relative"
                    key={`${index}-img`}
                    onClick={() => removeMobileVersionImages(index)}
                  >
                    <button className="position-absolute top-0 end-0  translate-middle rounded-circle btn btn-danger">
                      x
                    </button>
                    <Image src={URL.createObjectURL(img)} />
                  </div>
                );
              })}
            </div>
          </>
        )}
        {desktopVersionImages.length > 0 && (
          <>
            <h3 className="text-secondary mt-5 ">Desktop Version Photos</h3>
            <div className={styles.imageWrapper}>
              {desktopVersionImages.map((img, index) => {
                return (
                  <div
                    className="position-relative"
                    key={`${index}-img`}
                    onClick={() => removeDesktopVerionImages(index)}
                  >
                    <button className="position-absolute top-0 end-0  translate-middle rounded-circle btn btn-danger">
                      x
                    </button>
                    <Image src={URL.createObjectURL(img)} alt="" />
                  </div>
                );
              })}
            </div>
          </>
        )}
      </>
    );
  }
);

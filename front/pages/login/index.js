import { useState } from "react";
import axios from "axios";
export default function index() {
  const [login, setLogin] = useState(true);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (login) {
    }
  };

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div style={{ width: "300px" }} className="mt-5">
        <h2>{login ? "Login" : "Sign up"}</h2>
        <div className="form-group my-3">
          <label htmlFor="username">Username</label>
          <input
            id="username"
            className="form-control"
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group my-3">
          <label htmlFor="password">password</label>
          <input
            id="password"
            className="form-control"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button className="btn btn-primary my-3  w-100" onClick={handleSubmit}>
          {login ? "login" : "sign ups"}
        </button>
        <p>
          if Don't have account?{" "}
          <span
            className="text-primary pe-auto"
            onClick={() => setLogin(!login)}
          >
            {login ? "SignUp" : "Login"}
          </span>{" "}
          here
        </p>
      </div>
    </div>
  );
}

import styles from "./styles.module.scss";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import axios from "../../../axios.config";
import ReactMarkdown from "react-markdown";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export default function Post({ post }) {
  const [size, setSize] = useState(0);
  useEffect(() => {
    setSize(window.innerWidth);
  }, []);

  let date = new Date(post.date);
  let month = monthNames[date.getMonth()];
  let day = String(date.getDate()).padStart(2, "0");
  let year = date.getFullYear();
  let formatedDate = `${day} ${month}, ${year}`;

  return (
    <div className={styles.container}>
      <div className={styles.headerContainer}>
        <div className={styles.header}>
          <div>
            <h1
              style={
                post.fontStyle
                  ? {
                      fontFamily: post.fontStyle,
                      fontWeight: 300,
                    }
                  : { fontWeight: "900" }
              }
            >
              {post.title}
            </h1>
            <p>{formatedDate}</p>
            <div className={styles.shareIconsContainer}>
              <div className="addthis_inline_share_toolbox"></div>
            </div>
            <a className={styles.readMore} href="#reference">
              <span>&darr;</span>
              <p>Read More</p>
            </a>
          </div>
        </div>
      </div>

      <div className={styles.imageContainer}>
        <div className={styles.imageInnerContainer}>
          {size < 1000 ? (
            <>
              {post.mobileImages.normal.map((img, index) => {
                return (
                  <Image
                    key={`${index}-mobile-image`}
                    blurDataURL={post.mobileImages.blur[index]}
                    src={img}
                    width={1080}
                    height={1920}
                    quality={100}
                    objectFit="cover"
                    placeholder="blur"
                  />
                );
              })}
            </>
          ) : (
            <>
              {post.desktopImages.normal.map((img, index) => {
                return (
                  <Image
                    key={`${index}-img-desktop`}
                    blurDataURL={post.desktopImages.blur[index]}
                    src={img}
                    width={712}
                    height={400}
                    quality={100}
                    objectFit="cover"
                    placeholder="blur"
                  />
                );
              })}
            </>
          )}
        </div>
      </div>

      <div className={styles.sectionContainer} id="reference">
        <h1>Refernces</h1>

        <div>
          <ReactMarkdown>{post.body}</ReactMarkdown>
        </div>
      </div>
    </div>
  );
}

export async function getStaticPaths() {
  let { data } = await axios.get("blog");
  let paths = data.map((post) => {
    return {
      params: {
        id: post.slug,
      },
    };
  });
  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  let { data } = await axios.get(`blog/${params.id}`);
  return {
    props: {
      post: data,
    },
  };
}

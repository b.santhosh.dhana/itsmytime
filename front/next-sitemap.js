import { baseUrl } from "./axios.config.js";

module.exports = {
  siteUrl: baseUrl,
  generateRobotsTxt: true,
  exclude: ["/upload", "/login"],
  robotsTxtOptions: {
    policies: [
      {
        userAgent: "*",
        allow: "/",
      },
      {
        userAgent: "*",
        disallow: ["/upload", "/login"],
      },
    ],
  },
  additionalSitemaps: [
    `${baseUrl}/sitemap.xml`,
    `${baseUrl}/server-sitemap.xml`,
  ],
};

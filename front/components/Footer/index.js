import styles from './styles.module.scss'

export default function index() {
    return (
        <footer className={styles.container}>
            <div className={styles.innerContainer}>
                <p>Made With  <span>&#10084;</span> in Learning</p>
                <p><span>&copy;</span>itsmytime.com</p>
            </div>
        </footer>
    )
}

import styles from "./styles.module.scss";
import Image from "next/image";
import Link from "next/link";

export default function Featured({ posts }) {
  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        <h1>Latest Articles</h1>
        <div className={styles.cardsList}>
          {posts.map((post) => {
            return (
              <Link href={`/article/${post.slug}`} key={post.slug}>
                <div className={styles.card}>
                  <Image
                    className={styles.cardImage}
                    src={post.mobileImages.normal[0]}
                    blurDataURL={post.mobileImages.blur[0]}
                    quality={90}
                    width={1080}
                    height={1920}
                    placeholder="blur"
                  />
                </div>
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
}

import styles from "./styles.module.scss";
import Link from "next/link";

export default function index() {
  return (
    <nav className={styles.container}>
      <div className={styles.innerContainer}>
        <div className={styles.logo}>
          <Link href="/">
            <h1>
              Zero <br></br>Read.
            </h1>
          </Link>
        </div>
      </div>
    </nav>
  );
}

import styles from "./styles.module.scss";
import { useEffect, useRef, useState } from "react";

export default function Hero() {
  const [size, setSize] = useState(0);
  const [lottie, setLottie] = useState(null);
  const containerRef = useRef(null);

  useEffect(() => {
    import("lottie-web").then((lottie) => setLottie(lottie.default));
  }, []);

  useEffect(() => {
    setSize(window.innerWidth);
    if (lottie && containerRef.current) {
      const animation = lottie.loadAnimation({
        container: containerRef.current,
        renderer: "svg",
        loop: true,
        autoplay: true,
        path: "/data.json",
      });
      return () => animation.destroy();
    }
  }, [lottie]);

  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        <h1>Hey, I'm Dan.</h1>
        <h2>
          I will post my <span>curiosity</span> topics which my monkey mind
          demands.
        </h2>
        <p>
          You can read and see ideas and articles in various topics in concise
          bits of imformation. sometimes i will only questions without answers
        </p>
        {size > 1000 && (
          <div className={styles.lottie} ref={containerRef}></div>
        )}
      </div>
    </div>
  );
}

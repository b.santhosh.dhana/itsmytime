import axios from "axios";

export const localHost = "http://localhost:3000";
export const baseUrl = "http://localhost:8000";

const axiosInstance = axios.create({
  baseURL: baseUrl,
});

export default axiosInstance;

const mongoose = require("mongoose");

const connectDb = async () => {
  try {
    console.log("Connected to database successfully");
    return await mongoose.connect(process.env.MONGO_DB_URL);
  } catch (err) {
    console.log("ERROR: Datebase not connect: " + err);
  }
};

module.exports = connectDb;

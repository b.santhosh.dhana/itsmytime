const path = require("path");
module.exports = {
  MobileVersionUpload: path.join(__dirname, "../", "uploads/mobileVersion"),
  DesktopVersionUpload: path.join(__dirname, "../", "uploads/desktopVersion"),
  MobileVersionBlur: path.join(__dirname, "../", "blurImages/mobileVersion"),
  DesktopVersionBlur: path.join(__dirname, "../", "blurImages/desktopVersion"),
};

const express = require("express");
const app = express();
const cors = require("cors");
require("dotenv").config();
const connectDb = require("./config/db");
app.use(express.json());
app.use(cors());
app.use("/blog", require("./routes/blog"));
app.use("/auth", require("./routes/auth"));

const port = process.env.PORT || 3002;

app.listen(port, () => {
  console.log("app is running at " + port);
  connectDb();
});

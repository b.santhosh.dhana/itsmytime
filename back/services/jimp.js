const jimp = require("jimp");
const path = require("path");
const { getFileslist } = require("./utils");

let makeImageBlur = (srcPath, destPath) => {
  return new Promise((resolve, reject) => {
    const baseName = path.basename(srcPath);
    const savePath = path.join(destPath, `blur-${baseName}`);
    jimp.read(srcPath, (err, image) => {
      if (err) reject(err);
      image.resize(250, jimp.AUTO).blur(2).write(savePath);
      resolve(savePath);
    });
  });
};

// destFOlder C:\Users\my\Documents\ITS\itsmytime\back\blurImages

const createBlurImages = async (srcFolder, destFolder) => {
  try {
    let srcImagesList = await getFileslist(srcFolder);
    let blurImagePromises = srcImagesList.map((path) =>
      makeImageBlur(path, destFolder)
    );
    let blurImages = await Promise.all(blurImagePromises);
  } catch (err) {
    console.log(err);
  }
};

module.exports = createBlurImages;

const AWS = require("aws-sdk");
const fs = require("fs-extra");
const path = require("path");
const { getFileslist } = require("./utils");

const BUCKET_NAME = "my-small-bucket-new";

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_S3_ACCESS_KEY,
  secretAccessKey: process.env.AWS_S3_SECRET_KEY,
});

const upload = (file, key) => {
  return new Promise((resolve, reject) => {
    const fileStream = fs.createReadStream(file);
    const baseName = path.basename(file);
    let params = {
      Body: fileStream,
      Bucket: BUCKET_NAME,
      Key: `${key}/${baseName}`,
      ACL: "public-read",
    };
    s3.upload(params, (err, data) => {
      if (err) reject(err);
      else resolve(data.Location);
    });
  });
};

const uploadImagesToS3 = async (folder, key) => {
  try {
    let imagesListFiles = await getFileslist(folder);
    let s3UploadPromisesList = imagesListFiles.map((imgFile) =>
      upload(imgFile, key)
    );
    let data = await Promise.all(s3UploadPromisesList);
    return data;
  } catch (err) {
    return false;
  }
};

exports.uploadImagesToS3 = uploadImagesToS3;

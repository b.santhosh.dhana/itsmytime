const fs = require("fs-extra");
// [
//   FOLDER: 'C:\\Users\\my\\Documents\\ITS\\itsmytime\\back\\uploads/1.png',
// ]
const getFileslist = async (folder) => {
  return new Promise((resolve, reject) => {
    fs.readdir(folder, (err, files) => {
      if (err) reject(err);
      let filesList = files.map((file) => `${folder}/${file}`);
      resolve(filesList);
    });
  });
};

const deleteFilesInFolder = (folderPath) => {
  return new Promise((resolve, reject) => {
    fs.emptyDir(folderPath, (err) => {
      if (err) reject(err);
      resolve("deleted Succesfully");
    });
  });
};

const deletFilesInFolders = async (folder) => {
  try {
    if (Array.isArray(folder)) {
      folder.forEach(async (files) => {
        await deleteFilesInFolder(files);
      });
      return true;
    }
    await deleteFilesInFolder(folder);
  } catch (err) {
    return false;
  }
};

exports.getFileslist = getFileslist;
exports.deletFilesInFolders = deletFilesInFolders;

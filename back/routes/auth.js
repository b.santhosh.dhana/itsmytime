const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const User = require("../modals/user");

router.post("/signup", async (req, res) => {
  const { username, password } = req.body;
  res.status(400);
});

const user = {
  userName: process.env.USER_NAME,
  password: process.env.PASSWORD,
};

router.post("/login", (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) return res.status(401);

  if (username === user.userName && password === user.password) {
    const accessToken = jwt.sign(
      { username, role: "admin" },
      process.env.JWT_ACCESS_tOKEN_SECRET,
      { expiresIn: "1h" }
    );
    res.json(accessToken);
  } else {
    res.send("worng credentials");
  }
});

router.post("/refreshtoken", (req, res) => {});

module.exports = router;

const express = require("express");
const router = express.Router();
const multer = require("multer");
const Blog = require("../modals/blog");
const { uploadImagesToS3 } = require("../services/s3");
const createBlurImages = require("../services/jimp");
const { deletFilesInFolders } = require("../services/utils");
const path = require("path");
const folders = require("../config/foldersPaths");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (file.fieldname == "mobileVersionImages") {
      cb(null, "uploads/mobileVersion");
    } else {
      cb(null, "uploads/desktopVersion");
    }
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
const upload = multer({ storage });

router.get("/", async (req, res) => {
  let { limit = 10 } = req.query;
  try {
    let data = await Blog.find({}, "slug title date mobileImages", {
      sort: "-date",
      limit,
    });
    if (!data) throw new Error("something went wrong");
    res.json(data);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/:blogSlug", async (req, res) => {
  let slug = req.params.blogSlug;
  if (!slug) {
    return res.status(400).send("blogId is missed");
  }
  try {
    let data = await Blog.findOne({ slug });
    res.json(data);
  } catch (err) {
    res.status(500).send("something went wrong");
  }
});

let uploadMiddleware = upload.fields([
  { name: "mobileVersionImages", maxCount: 10 },
  { name: "desktopVersionImages", maxCount: 10 },
]);

router.post("/upload", uploadMiddleware, async (req, res) => {
  const { title, markDown, secretKey, fontStyle } = req.body;
  let { mobileVersionImages, desktopVersionImages } = req.files;

  if (!title || !markDown) {
    return res.status(400).send("Data is missed");
  }

  if (secretKey !== process.env.PASSWORD) {
    return res.status(401).send("unAuthoried");
  }

  try {
    // blur images for DesktopVersion / Mobile Version
    await createBlurImages(
      folders.DesktopVersionUpload,
      folders.DesktopVersionBlur
    );
    await createBlurImages(
      folders.MobileVersionUpload,
      folders.MobileVersionBlur
    );

    // upload uploaded images to s3
    let normalImgUrlsForMobile = await uploadImagesToS3(
      folders.MobileVersionUpload,
      title
    );
    let normalImgUrlsForDesktop = await uploadImagesToS3(
      folders.DesktopVersionUpload,
      title
    );
    // upload blur images to s3
    let blurImgUrlsForMobile = await uploadImagesToS3(
      folders.MobileVersionBlur,
      title
    );
    let blurImgUrlsForDesktop = await uploadImagesToS3(
      folders.DesktopVersionBlur,
      title
    );

    if (
      !normalImgUrlsForMobile ||
      !normalImgUrlsForDesktop ||
      !blurImgUrlsForMobile ||
      !blurImgUrlsForDesktop
    ) {
      throw new Error("Failed to upload to S3");
    }
    // save to db
    const blog = new Blog({
      title,
      body: markDown,
      mobileImages: {
        normal: normalImgUrlsForMobile,
        blur: blurImgUrlsForMobile,
      },
      desktopImages: {
        normal: normalImgUrlsForDesktop,
        blur: blurImgUrlsForDesktop,
      },
      fontStyle,
    });
    const result = await blog.save();

    res.status(200).json(result);
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: err });
  } finally {
    // delete saved Images
    const {
      MobileVersionUpload,
      DesktopVersionUpload,
      MobileVersionBlur,
      DesktopVersionBlur,
    } = folders;
    await deletFilesInFolders([
      MobileVersionUpload,
      DesktopVersionUpload,
      MobileVersionBlur,
      DesktopVersionBlur,
    ]);
  }
});

module.exports = router;

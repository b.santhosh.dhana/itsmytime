const mongoose = require("mongoose");
const { Schema } = mongoose;
const slugify = require("slugify");

const blogSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now(),
  },
  author: String,
  body: {
    type: String,
    required: true,
  },
  mobileImages: {
    normal: [String],
    blur: [String],
  },
  desktopImages: {
    normal: [String],
    blur: [String],
  },
  slug: {
    type: String,
    required: true,
    unique: true,
  },
  fontStyle: String,
});

blogSchema.pre("validate", function (next) {
  this.slug = slugify(this.title, { strict: true, lower: true, trim: true });
  next();
});

const Blog = mongoose.model("Blog", blogSchema);

module.exports = Blog;
